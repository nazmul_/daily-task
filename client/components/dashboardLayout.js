import Link from 'next/link'
import { Layout, Menu } from 'antd';

const { Header } = Layout;

const dashboardLayout = ({ children }) => {
  return (
    <div>
      <Layout className="layout">
        <Header>
          <Menu theme="dark" mode="horizontal">
            <Menu.Item key="home"><Link href="/dashboard">DailyTask</Link></Menu.Item>
            <Menu.Item key="alltask"><Link href="/alltask">Tasklist</Link></Menu.Item>
            <Menu.Item key="newtask"><Link href="/newtask">New Task</Link></Menu.Item>
            <Menu.Item key="logout"><Link href="/">Logout</Link></Menu.Item>
          </Menu>
        </Header>
      </Layout>

      <main>
        <div className="uk-container">
          {children}
        </div>
      </main>
    </div>
  )
}

export default dashboardLayout
