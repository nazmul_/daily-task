import Link from 'next/link'
import { Layout, Menu } from 'antd';

const { Header } = Layout;

const homepageLayout = ({ children }) => {
  return (
    <div>
      <Layout className="layout">
        <Header>
          <Menu theme="dark" mode="horizontal">
            <Menu.Item key="home"><Link href="/">DailyTask</Link></Menu.Item>
            <Menu.Item key="faq"><Link href="/faq">Faq</Link></Menu.Item>
            <Menu.Item key="login"><Link href="/login">Login</Link></Menu.Item>
            <Menu.Item key="signup"><Link href="/signup">Registration</Link></Menu.Item>
          </Menu>
        </Header>
      </Layout>

      <main>
        <div className="uk-container">
          {children}
        </div>
      </main>
    </div>
  )
}

export default homepageLayout
