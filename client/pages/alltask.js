// import { Table, Space } from 'antd';
import Head from 'next/head'
import { Component } from 'react';
import axios from 'axios'
import Router, { withRouter } from 'next/router'

import EditModal from './editmodal'
import DeleteModal from './deletemodal'

import DashboardLayout from '../components/dashboardLayout'

class alltask extends Component {

  constructor() {
    super()

    this.state = {
      task: [],
      visible: false,
      data: {},
      deleteModalVisibility: false
    }

    this.deleteWarningTitle = "Do you want to delete?"
  }

  componentDidMount() {
    // console.log(this)
    axios.get('http://localhost:1337/alltask')
      .then(res => {
        // console.log(res)
        this.setState({ task: res.data })
      })
      .catch(err => console.log(err))
  }

  deleteModalOnDone = (obj) => {
    axios.delete(`http://localhost:1337/alltask/${obj.id}`)
      .then(res => {
        console.log(res.status)
        if (res.status == 200) {
          let dlt_id = this.state.task.filter(a => a.id != obj.id)
          this.setState({ task: dlt_id, deleteModalVisibility: false })
        }
      })
  }

  deleteHandler = (id) => {
    this.setState({
      data: { id: id },
      deleteModalVisibility: true
    })
  }

  updateHandler = (item) => {
    this.setState({ data: item, visible: true })
  }

  onCreate = (values) => {
    axios.put(`http://localhost:1337/alltask/${values.id}`, { body: values })
      .then(res => {
        if (res.status == 200) {
          let item = this.state.task.find(item => item.id == values.id)
          item.title = values.title;
          item.description = values.description;
          this.setState({ task: this.state.task, visible: false })
        }
      }
    )
  }

  render() {
    return (
      <div>
        <Head>
          <title>Tasklists</title>
        </Head>
        <DashboardLayout>
          <h1 className="uk-margin-top">All Tasks</h1><hr />
          <table className="uk-table uk-table-striped">
            <thead>
              <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Update</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              {this.state.task.map((item, key) => {
                return (
                  <tr key={key}>
                    <td>{item.title}</td>
                    <td>{item.description}</td>
                    <td>
                      <button className="uk-input uk-form-success" onClick={this.updateHandler.bind(this, item)}>Update</button>
                    </td>
                    <td>
                      <button className="uk-input uk-form-danger" onClick={this.deleteHandler.bind(this, item.id)}>Delete</button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </DashboardLayout>

        <EditModal
          visible={this.state.visible}
          onCreate={this.onCreate}
          onCancel={() => {
            this.setState({ visible: false })
          }}
          info={this.state.data}
        />

        <DeleteModal
          info={this.state.data}
          title={this.deleteWarningTitle}
          onDone={this.deleteModalOnDone}
          onCancel={() => {
            this.setState({
              deleteModalVisibility: false,
            })
          }}
          visible={this.state.deleteModalVisibility}
        />
      </div>
    )
  }
}

export default withRouter(alltask)
