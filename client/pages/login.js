import { Form, Input, Button } from 'antd';
import axios from 'axios';
import Head from 'next/head';
import HomepageLayout from '../components/homepageLayout'
import { useRouter } from 'next/router'

const tailLayout = {
  wrapperCol: { offset: 12, span: 16 },
};

const Demo = () => {
  const router = useRouter()

  const onFinish = values => {
    console.log('Success:', values);
    axios.post('http://localhost:1337/login', { body: values })
      .then(res => {
        console.log(res.data.auth)

        if (res.data.auth === 'Success') {
          router.push('/dashboard')
          alert('Login successful')
        } else {
          router.push('/login')
          alert('Incorrect email or password')
        }
      })
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Form
      name="basic"
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item label="Email" name="email" rules={[{ required: true, message: 'Please input your email!' }]}><Input /></Form.Item>

      <Form.Item label="Password" name="password" rules={[{ required: true, message: 'Please input your password!' }]}><Input.Password /></Form.Item>

      <Form.Item {...tailLayout}>
        <Button className="uk-margin-top" type="primary" htmlType="submit">Login</Button>
      </Form.Item>
    </Form>
  );
};


const login = () => {

  return (
    <div>
      <Head>
        <title>Login to your account</title>
      </Head>
      <HomepageLayout>
        <h1 className="uk-margin-top">Login</h1>
        <Demo />
      </HomepageLayout>
    </div>
  )
}

export default login
