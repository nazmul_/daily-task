import HomepageLayout from '../components/homepageLayout'
import Head from 'next/head'
import Link from 'next/link'
import { Button } from 'antd';

const antIndex = () => {
  return (
    <div>
      <Head>
        <title>DailyTask !</title>
      </Head>
      <HomepageLayout />
      <div className="uk-container">
        <h1 className="uk-margin-top uk-margin-left">Welcome to next js project</h1>
        <p className="uk-margin-left">This is a small nextjs project</p>
        <Button className="uk-margin-left" type="primary"><Link href="/login">Login</Link></Button>
        <Button className="uk-margin-left" type="dashed"><Link href="/signup">Registration</Link></Button>
      </div>
    </div>
  )
}

export default antIndex
