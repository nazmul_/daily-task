import React, { useState } from 'react';
import { Button, Modal, Form, Input } from 'antd';
import { useEffect } from 'react'

const EditModal = ({ visible, onCreate, onCancel, info }) => {
  const [form] = Form.useForm();
  useEffect(() => {
    // console.log(form)
    form.setFieldsValue({
      title: info.title,
      description: info.description,
    });
  })
  return (
    <Modal
      visible={visible}
      title="Edit Task"
      okText="Update"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            form.resetFields();
            values['id'] = info.id
            onCreate(values);
          })
          .catch((info) => {
            console.log('Validate Failed:', info);
          });
      }}
    >
      <Form
        form={form}
        layout="vertical"
        name="form_in_modal"
        initialValues={{
          modifier: 'public',
        }}
      >
        <Form.Item
          name="title"
          label="Title"
        >
          <Input type="text" />
        </Form.Item>
        <Form.Item name="description" label="Description">
          <Input type="textarea" />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default EditModal
