import { Form, Input, Button } from 'antd'
import axios from 'axios';
import Head from 'next/head'
import DashboardLayout from '../components/dashboardLayout'
import { useRouter } from 'next/router'
import { useEffect } from 'react'
import stateData from './state'

const { TextArea } = Input;

const tailLayout = {
  wrapperCol: { offset: 12, span: 16 },
};

const Demo = (props) => {
  const router = useRouter()
  const [form] = Form.useForm();

  const onFinish = values => {
    console.log('Success:', values);
    axios.put('http://localhost:1337/alltask/' + stateData.item.id, { body: values })
    .then(res => {
      router.push('/alltask')
    })
    
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  }

  useEffect(() => {
    console.log(props)
    form.setFieldsValue({
      title: props.item.title,
      description: props.item.description,
    });
  })

  return (
    <Form
      form={form}
      name="basic"
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item name="title" label="Title" rules={[{ required: true, },]}><Input value={props.item.title} /></Form.Item>
      <Form.Item name="description" label="Description" rules={[{ required: true, },]}><TextArea /></Form.Item>

      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">Edit Task</Button>
      </Form.Item>
    </Form>
  )
}

const edittask = () => {

  useEffect(() => {
    console.log(stateData.item)
  })

  return (
    <div>
      <Head>
        <title> Edit Task</title>
      </Head>
      <DashboardLayout>
        <h1 className="uk-margin-top">Edit Task</h1>
        <Demo item ={stateData.item} />
      </DashboardLayout>
    </div>
  )
}

export default edittask
