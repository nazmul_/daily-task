import DashboardLayout from '../components/dashboardLayout'
import Head from 'next/head'
import Link from 'next/link'
import { Button } from 'antd';

const dashboard = () => {
  return (
    <div>
      <Head>
        <title>DailyTask !</title>
      </Head>
      <DashboardLayout />
      <div className="uk-container">
        <h1 className="uk-margin-top uk-margin-left">Welcome to next js project</h1>
        <p className="uk-margin-left">This is a small nextjs project</p>
        <Button className="uk-margin-left" type="primary"><Link href="/alltask">Tasklist</Link></Button>
        <Button className="uk-margin-left" type="dashed"><Link href="/newtask">New Task</Link></Button>
      </div>
    </div>
  )
}

export default dashboard
