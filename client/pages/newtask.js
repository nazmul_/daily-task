import { Form, Input, Button } from 'antd'
import axios from 'axios';
import Head from 'next/head'
import DashboardLayout from '../components/dashboardLayout'
import { useRouter } from 'next/router'

const { TextArea } = Input;

const tailLayout = {
  wrapperCol: { offset: 12, span: 16 },
};

const Demo = () => {
  const router = useRouter()

  const onFinish = values => {
    console.log('Success:', values);
    axios.post('http://localhost:1337/newtask', { body: values })
    router.push('/alltask')
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Form
      name="basic"
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item name="title" label="Title" rules={[{ required: true, },]}><Input /></Form.Item>
      <Form.Item name="description" label="Description" rules={[{ required: true, },]}><TextArea /></Form.Item>

      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">Add Task</Button>
      </Form.Item>
    </Form>
  )
}

const newtask = () => {

  return (
    <div>
      <Head>
        <title>Add New Task</title>
      </Head>
      <DashboardLayout>
        <h1 className="uk-margin-top">Add New Task</h1>
        <Demo />
      </DashboardLayout>
    </div>
  )
}

export default newtask
