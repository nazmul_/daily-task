import { Form, Input, Button } from 'antd';
import Head from 'next/head'
import HomepageLayout from '../components/homepageLayout'
import axios from 'axios'
import { useRouter } from 'next/router'

const tailLayout = {
  wrapperCol: { offset: 12, span: 16 },
};

const Demo = () => {
  const router = useRouter()
  const onFinish = values => {
    console.log('Success:', values);
    axios.post('http://localhost:1337/registration', { body: values })
    router.push('/dashboard')
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Form name="basic" onFinish={onFinish} onFinishFailed={onFinishFailed}>

      <Form.Item label="Username" name="username" rules={[{ required: true, message: 'Please input your username!' }]}>
        <Input />
      </Form.Item>

      <Form.Item label="Email" name="email" rules={[{ required: true, message: 'Please input your email!' }]}>
        <Input />
      </Form.Item>

      <Form.Item label="Password" name="password" rules={[{ required: true, message: 'Please input your password!' }]}>
        <Input.Password />
      </Form.Item>

      <Form.Item {...tailLayout}>
        <Button className="uk-margin-top" type="primary" htmlType="submit">Registration</Button>
      </Form.Item>

    </Form>
  );
};


const singup = () => {

  return (
    <div>
      <Head>
        <title>Signup for an account</title>
      </Head>
      <HomepageLayout>
        <h1 className="uk-margin-top">Registration</h1>
        <Demo />
      </HomepageLayout>
    </div>
  )
}

export default singup
