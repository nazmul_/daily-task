import React from 'react';
import { Modal, Button, Space } from 'antd';
const { confirm } = Modal;

const DeleteModal = (props) => {
  return (
    <Modal
      visible={props.visible}
      title="Confirmation"
      okText="delete"
      cancelText="Cancel"
      onCancel={props.onCancel}
      onOk={() => {
        props.onDone(props.info)
      }}
    >
      <p>{props.title}</p>
    </Modal>
  );

};

export default DeleteModal