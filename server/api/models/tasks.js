module.exports = {

  attributes: {
    title: {
      type: 'string'
    },
    description: {
      type: 'string'
    }
  }
}