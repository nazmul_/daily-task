/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  create(req, res){
    console.log(req.body)
    // console.log(req.body.body.name)
    // console.log(req.body.body.username)

    User.create({
      username: req.body.body.username,
      email: req.body.body.email,
      password: req.body.body.password,
    })
    .then(user => {
      return res.ok(user, {message: 'success'})
    })
    .catch(err => console.log(err))
  },

  find(req, res) {
    console.log(req.body)
    console.log(req.body.body.email)
    console.log(req.body.body.password)

    User.find({ "email": req.body.body.email, "password": req.body.body.password })
    .then(result => {
      console.log(result)
      if(result.length > 0) {
        res.ok({ auth: 'Success', message: 'auth ok' })
      } else {
        res.ok(result, {auth: 'Failed', message: 'incorrect email or password'})
      }
    })
  }

};

