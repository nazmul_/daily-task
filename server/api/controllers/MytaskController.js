/**
 * MytaskController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

// const task = require("../models/Mytask");

module.exports = {

  find(req, res) {
    Mytask.find().sort([
      { createdAt: 'DESC' },
    ])
    .then(tasks => {
      res.ok(tasks, {message: 'success'})
    })
    .catch(err => {
      res.ok(err)
    })
  },

  create(req, res) {
    console.log(req.body)
    // console.log(req.body.body.title)
    // console.log(req.body.body.description)

    Mytask.create({
      title: req.body.body.title,
      description: req.body.body.description
    })
    .then(task => {
      res.ok(task, {message: 'data insert success'})
    })
  },

  delete(req, res) {
    console.log(req.body)

    Mytask.destroy({
      id: req.params.id
    })
    .then(dltid => {
      res.ok(dltid, {message: 'id has been deleted'})
    })
    // res.redirect('http://localhost:5002/alltask')
  },

  update(req, res) {
    console.log(req)
    Mytask.update({
      id: req.params.id
    })
    .set({ title: req.body.body.title, description: req.body.body.description })
    .then(edtsk => {
      res.ok(edtsk)
    })
    .catch(err => {
      res.ok(err)
    })
  }

};

